#!/bin/bash

composer update
cp .env.example .env
chmod -R 777 storage
php artisan cache:clear
php artisan key:generate