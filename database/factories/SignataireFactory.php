<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Signataire::class, function (Faker $faker) {
    return [
        'key' => $faker->regexify('[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}'),
        'certificat' => $faker->text(200),
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'mail' => $faker->email,
        'enterprise' => $faker->name,
        'signature' => $faker->imageUrl(),
        'date_signature' => $faker->dateTime,
    ];
});
