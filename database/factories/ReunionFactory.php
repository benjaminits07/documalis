<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Reunion::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->text(200),
        'date_creation' => $faker->dateTime(),
        'url_complementaire' => $faker->domainName,
        'delai_signature' => $faker->dateTime(),
    ];
});
