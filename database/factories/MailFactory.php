<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Mail::class, function (Faker $faker) {
    return [
        'subject' => $faker->name,
        'message' =>$faker->text(200),
        'relance'=>$faker->randomDigit,
    ];
});
