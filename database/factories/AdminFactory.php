<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Admin::class, function (Faker $faker) {
    return [
        'key' => $faker->regexify('[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}'),
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'mail' => $faker->email,
    ];
});
