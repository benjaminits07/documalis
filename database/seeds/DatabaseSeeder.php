<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminsTableSeeder::class);
        $this->call(MailsTableSeeder::class);
        $this->call(PdfsTableSeeder::class);
        $this->call(ReunionsTableSeeder::class);
        $this->call(SignatairesTableSeeder::class);
        $this->command->info('✔ Seed successful');
    }
}
