<?php

use Illuminate\Database\Seeder;

class PdfsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Pdf::class, 100)->create();
    }
}