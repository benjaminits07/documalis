<?php

use Illuminate\Database\Seeder;

class SignatairesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Signataire::class, 100)->create();
    }
}