<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
        <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif
            <!-- Signature canvas -->
            <div id="signature-pad" class="signature-pad">
                <div class="signature-pad--body">
                    <canvas></canvas>
                </div>
                <div class="signature-pad--footer">
                    <div class="description">Signer dans le rectangle ci-dessus</div>
                    <div class="signature-pad--actions">
                        <div>
                            <button type="button" class="button clear" data-action="clear">Effacer</button>
                        </div>
                        <div>
                            <button type="button" class="button save" data-action="save-png">Enregistrer l'image (PNG)</button>
                            <button type="button" class="button save" data-action="save-jpg">Enregistrer l'image (JPG)</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
