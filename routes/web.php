<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\Mail\Confirmation;
use Illuminate\Support\Facades\Mail;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/signature', function () {
    return view('signature');
});

Route::get('/test', function () {
    $test = \App\pdf::find(1)->reunion();
    dd($test);
    return $test;
});

Route::get('/send', function () {
    Mail::to('benjamin.denie@gmail.com')->send(new Confirmation);
    return redirect('/confirm');
});

Route::get('/confirm', function() {
    return view('mails.confirm');
});

Route::get('/pdf-parser-demo', function () {
    $filename =  '../storage/pdf/convention_dhonoraires_au_forfait.pdf';
    $rawdata = file_get_contents($filename);
    if ($rawdata === false) {
        $this->Error('Unable to get the content of the file: '.$filename);
    }
    // configuration parameters for parser
    $cfg = array(
        'die_for_errors' => false,
        'ignore_filter_decoding_errors' => true,
        'ignore_missing_filter_decoders' => true,
    );
    try {
        // parse PDF data
        $pdf = new TCPDF_PARSER($rawdata, $cfg);
    } catch (Exception $e) {
        die($e->getMessage());
    }
    // get the parsed data
    $data = $pdf->getParsedData();
    // release some memory
    unset($rawdata);
   dd($data);
    return $data;
});
