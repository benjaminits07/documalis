<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reunion extends Model
{
    /**
     * Get the pdf record associated with the reunion.
     */
    public function pdf()
    {
        return $this->hasOne('App\Models\Pdf');
    }

    /**
     * Get the mail record associated with the reunion.
     */
    public function mail()
    {
        return $this->hasOne('App\Models\Mail');
    }

    /**
     * Get the admin record associated with the reunion.
     */
    public function admin()
    {
        return $this->hasOne('App\Models\Admin');
    }

    /**
     * The signataire that belong to the reunion.
     */
    public function signataire()
    {
        return $this->belongsToMany('App\Models\Signataire');
    }
}
