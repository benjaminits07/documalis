<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pdf extends Model
{
    /**
     * Get the phone record associated with the reunion.
     */
    public function reunion()
    {
        return $this->hasOne('App\Models\Reunion');
    }
}
