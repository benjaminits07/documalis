<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    /**
     * Get the reunion record associated with the reunion.
     */
    public function reunion()
    {
        return $this->hasOne('App\Models\Reunion');
    }
}
