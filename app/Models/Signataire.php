<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Signataire extends Model
{
    /**
     * The reunion that belong to the signataire.
     */
    public function reunion()
    {
        return $this->belongsToMany('App\reunion');
    }
}
